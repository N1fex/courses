import $ from 'jquery'
import '@/style/app.scss'

import Course from './components/Course'

$(document).ready(function(){

  window.bd = {
    courses: []
  }

  let blockForm = $('.form');

  let coursesList = $('.courses-list');

  let createButton = $('.manipulators__button_create');
  let cancelButton = $('.manipulators__button_cancel');

  let courseTitleInput = $('.course-title__input');
  let courseImgInput = $('.course-img__input');
  let coursePriceInput = $('.course-price__input');
  let courseDescriptionTextarea = $('.course-description__textarea');

  const routePage = page => {
    $('.component').css('display', 'none');
    $(`[data-page=${page}]`).css('display', 'block');
  }

  $('[data-nav]').click(function(){
    routePage($(this).attr('data-nav'));
    $('.nav__item').removeClass('active');
    $(this).addClass('active');
  })

  // let formInputs = $(".form input");
  // let fieldsError = [];
  // let errText = '';
  //
  // let validLetters = /^[A-Za-zА-Яа-я-]{3,}$/;
  // let validNumbers = /^\d$/;
  //
  // for (let i=0; i<formInputs.length; i++){
  //   if($(formInputs[i]).attr('name') === 'title'){
  //     !validLetters.test(formInputs[i].value) ? fieldsError.push($(formInputs[i]).attr('data-name')) : null;
  //   }
  //   if($(formInputs[i]).attr('name') === 'price'){
  //     !validNumbers.test(formInputs[i].value) ? fieldsError.push($(formInputs[i]).attr('data-name')) : null;
  //   }
  // }
  //
  // if(fieldsError.length){
  //   errText = 'Поле ' + fieldsError.join(', ') + ' неверно заполнено';
  // }
  //
  // if(errText.length){
  //   $('.window__message').addClass('active').text(errText);
  // } else if (!fieldsError.length){
  //   $('.window__message').removeClass('active');
  // }

  let newCourse = () => {
    let course = bd.courses[bd.courses.length-1];
    course = new Course(course)
    coursesList.append(course.render());
    course.functionality();
  }

  let addCourse = (title, img, price, description) => {
    if (!!title.length && !!img.length && !!price.length && !!description.length){
      bd.courses.push({
        id: +new Date,
        title,
        img,
        price,
        description,
        order: false
      });
      newCourse();
    }
    blockForm.css('display', 'none');
  }

  let clearInputs = () => {
    courseTitleInput.val('');
    courseImgInput.val('');
    coursePriceInput.val('');
    courseDescriptionTextarea.val('');
  }

  createButton.on("click", function(){
    addCourse(courseTitleInput.val().trim(), courseImgInput.val().trim(), coursePriceInput.val().trim(), courseDescriptionTextarea.val().trim());
    clearInputs();
  });

  cancelButton.on("click", function(){
    $(this).closest(blockForm).css('display', 'none');
  });

})