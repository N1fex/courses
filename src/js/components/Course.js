import $ from "jquery";

let coursesCart = $('.courses-cart');

export default class Course{
  constructor(course){
    this.id = course.id;
    this.title = course.title;
    this.img = course.img;
    this.price = course.price;
    this.description = course.description;
    this.order = course.order;
  }

  render(){
    return `
      <div class="course" id=${this.id} style="background-image: url('${this.img}'); background-size: 100% 100%">
        <span class="course__title">${this.title}</span>
        <div class="course__separator"></div>
        <span class="course__price">${this.price}<span class="currency"> р.</span></span>
        <span class="course__description">${this.description}</span>
        <div class="course__buttons buttons">
          <button class="buttons__remove">Delete</button>
          <button class="buttons__in-cart">In cart</button>
        </div>
      </div>
    `
  }

  functionality(){
    // кнопка удаления курса
    this.eventListenerForObject(this.id, '.buttons__remove', this.removeFunc);
    // кнопка "в корзину"
    this.eventListenerForObject(this.id, '.buttons__in-cart', this.inCartFunc);
  }

  eventListenerForObject(id, selector, func){
    $(`#${id} ${selector}`).on('click', func.bind(this));
  }

  eventListenerForSelector(id, selector, func){
    $(`#${id} ${selector}`).on('click', func);
  }

  removeFunc() {
    console.log($('.course#' + this.id));
  }

  inCartFunc(){
    bd.courses = bd.courses.map(course => {
      if(course.id === this.id){
        course.order = !course.order;
        let courseThis = new Course(course)
        coursesCart.append(courseThis.render())
      }
      return course
    })
  }

}